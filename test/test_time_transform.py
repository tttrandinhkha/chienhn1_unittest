import datetime
from src.time_transform import time_transform

def test_time_transform():
    assert time_transform(1563635466) == datetime.datetime(2019, 7, 20, 15, 11, 6)

def test_time_transform_wrong_data_type():
    assert time_transform(1563635466) != "2022/05/25"
