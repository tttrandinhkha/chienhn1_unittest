import pytest
from src.flatten_json import get_field_value

# Đi vào 'except' với đầu vào là json_input1 (vì sea_level, grnd_level và wind_gust không tồn tại)
json_input1 = {'coord': {'lon': -0.1257, 'lat': 51.5085},
 'weather': [{'id': 803,
   'main': 'Clouds',
   'description': 'broken clouds',
   'icon': '04n'}],
 'base': 'stations',
 'main': {'temp': 46.42,
  'feels_like': 44.53,
  'temp_min': 42.78,
  'temp_max': 48.81,
  'pressure': 1015,
  'humidity': 93},
 'visibility': 10000,
 'wind': {'speed': 4.27, 'deg': 276},
 'clouds': {'all': 69},
 'dt': 1654049744,
 'sys': {'type': 2,
  'id': 2019646,
  'country': 'GB',
  'sunrise': 1654055356,
  'sunset': 1654114062},
 'timezone': 3600,
 'id': 2643743,
 'name': 'London',
 'cod': 200}


# Không vào 'except' (tồn tại đủ các trước sea_level, grnd_level, wind_gust)
json_input2 = {'coord': {'lon': 105.8412, 'lat': 21.0245},
 'weather': [{'id': 803,
   'main': 'Clouds',
   'description': 'broken clouds',
   'icon': '04d'}],
 'base': 'stations',
 'main': {'temp': 86,
  'feels_like': 94.64,
  'temp_min': 86,
  'temp_max': 86,
  'pressure': 1005,
  'humidity': 69,
  'sea_level': 1005,
  'grnd_level': 1004},
 'visibility': 10000,
 'wind': {'speed': 4.72, 'deg': 180, 'gust': 6.42},
 'clouds': {'all': 79},
 'dt': 1654052368,
 'sys': {'type': 1,
  'id': 9308,
  'country': 'VN',
  'sunrise': 1654035296,
  'sunset': 1654083253},
 'timezone': 25200,
 'id': 1581130,
 'name': 'Hanoi',
 'cod': 200}



dict_temp = get_field_value(json_input1)
def test_extract_json_main_sea_level():
	assert dict_temp['main_sea_level'] == None

dict_temp_1 = get_field_value(json_input2)
def test_extract_json_main_sea_level_failed():
	assert dict_temp_1['main_sea_level'] != None

dict_temp_2 = get_field_value(json_input1)
def test_extract_json_wind_gust():
	assert dict_temp_1['wind_gust'] != None

dict_temp_3 = get_field_value(json_input2)
def test_extract_json_grnd_level():
	assert dict_temp_3['main_grnd_level'] != None
