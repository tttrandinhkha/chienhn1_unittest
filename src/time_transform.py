import datetime
def time_transform(time_in_seconds):
    return datetime.datetime.fromtimestamp(time_in_seconds)