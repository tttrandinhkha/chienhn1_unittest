# create a dictionary containing attribute names as keys, 
#   and their corresponding values returned by call_weather_api function:
def get_field_value(data):
    data_dict = {}

    data_dict['coord_lon'] = data['coord']['lon']
    data_dict['coord_lat'] = data['coord']['lat']
    data_dict['weather_id'] = data['weather'][0]['id']
    data_dict['weather_main'] = data['weather'][0]['main']
    data_dict['weather_description'] = data['weather'][0]['description']
    data_dict['weather_icon'] = data['weather'][0]['icon']
    data_dict['base'] = data['base']
    data_dict['main_temp'] = data['main']['temp']
    data_dict['main_feels_like'] = data['main']['feels_like']
    data_dict['main_temp_min'] = data['main']['temp_min']
    data_dict['main_temp_max'] = data['main']['temp_max']
    data_dict['main_pressure'] = data['main']['pressure']
    data_dict['main_humidity'] = data['main']['humidity']
    try:
        data_dict['main_sea_level'] = data['main']['sea_level']
    except:
        data_dict['main_sea_level'] = None
    try:
        data_dict['main_grnd_level'] = data['main']['grnd_level']
    except:
        data_dict['main_grnd_level'] = None
    data_dict['visibility'] = data['visibility']
    data_dict['wind_speed'] = data['wind']['speed']
    data_dict['wind_deg'] = data['wind']['deg']
    try:
        data_dict['wind_gust'] = data['wind']['gust']
    except:
        data_dict['wind_gust'] = None
    data_dict['clouds_all'] = data['clouds']['all']
    data_dict['dt'] = data['dt']
    data_dict['sys_type'] = data['sys']['type']
    data_dict['sys_id'] = data['sys']['id']
    data_dict['sys_country'] = data['sys']['country']
    data_dict['sys_sunrise'] = data['sys']['sunrise']
    data_dict['sys_sunset'] = data['sys']['sunset']
    data_dict['timezone'] = data['timezone']
    data_dict['id'] = data['id']
    data_dict['name'] = data['name']
    data_dict['cod'] = data['cod']

    return data_dict